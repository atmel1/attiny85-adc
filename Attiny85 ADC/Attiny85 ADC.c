/*
 * Attiny85_ADC.c
 *
 * Created: 3/21/2015 1:20:49
 *  Author: Brandy
 */ 

#define F_CPU 1000000UL
#include <avr/io.h>
#include <util/delay.h>

void initADC()
{
  /* this function initialises the ADC
        ADC Notes
	Prescaler
	ADC Prescaler needs to be set so that the ADC input frequency is between 50 - 200kHz.
	Example prescaler values for various frequencies
	Clock   Available prescaler values
   ---------------------------------------
	 1 MHz   8 (125kHz), 16 (62.5kHz)
	 4 MHz   32 (125kHz), 64 (62.5kHz)
	 8 MHz   64 (125kHz), 128 (62.5kHz)
	16 MHz   128 (125kHz)
   below example set prescaler to 128 for mcu running at 8MHz
  */
  ADMUX =   (1 << ADLAR) |     // left shift result
            (0 << REFS1) |     // Sets ref. voltage to VCC, bit 1
            (0 << REFS0) |     // Sets ref. voltage to VCC, bit 0
            (0 << MUX3)  |     // use ADC2 for input (PB4), MUX bit 3
            (0 << MUX2)  |     // use ADC2 for input (PB4), MUX bit 2
            (1 << MUX1)  |     // use ADC2 for input (PB4), MUX bit 1
            (0 << MUX0);       // use ADC2 for input (PB4), MUX bit 0
  ADCSRA =  (1 << ADEN)  |     // Enable ADC
            (0 << ADPS2) |     // set prescaler to 64, bit 2
            (1 << ADPS1) |     // set prescaler to 64, bit 1
            (1 << ADPS0);      // set prescaler to 64, bit 0
}
int main( void )
{
  initADC();
  while(1)
  {
    ADCSRA |= (1 << ADSC);         // start ADC measurement
    while (ADCSRA & (1 << ADSC) ); // wait till conversion complete
    if (ADCH > 128)
    {
      // ADC input voltage is more than half of VCC
    } 
    else 
    {
      // ADC input voltage is less than half of VCC
    }
  }
  return 0;
}